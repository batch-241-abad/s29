/*db.users.insertMany([
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21, 
		contact: {
			phone: "87654321",
			email: "janedoe@gmail.com"
		}, 
		courses: [ "CSS", "Javascript", "Python" ], 
		department: "HR"
	},
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76, 
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		}, 
		courses: [ "Python", "React", "PHP" ], 
		department: "HR"
	},
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
	{
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
    }
])

*/




/*
	QUERY OPERATORS

	$gt or $gte operator

	-allows us to have documents that have field number values greater than or equal to a spcified value

	SYNTAX:
		db.collectionName.find({field: {$gt: value}})
		db.collectionName.find({field: {$gte: value}})
*/


// greater than 65
db.users.find({age:{ $gt: 65}});


// greater than or equal to 65
db.users.find({age:{ $gte: 65}});


/*

	$lt or $lte operator


	-allows us to find documents that have field number less than or equal to a specified value

	SYNTAX:
		db.collectionName.find({field: {$lt: value}})
		db.collectionName.find({field: {$lte: value}})
*/


// less than 76
db.users.find({age:{ $lt: 76}});


// less than or equal to 76
db.users.find({age:{ $lte: 76}});


/*
	NE OPERATOR

	-allows us to find docs that have a specified value that are not equal to a specified value

	SYNTAX:
		db.collectionName.find({field: {$ne:value}});

*/

// not equal to 82
	db.users.find({field: {$ne:82}});

/*
	LOGICAL QUERY OPERATORS

	$or , $and
*/

/*
	allows us to find documents that match a single criteria from multiple search criteria provided
	
	SYNTAX:
		db.collectionName.find({$or: [{fieldA: "valueA"}, {fieldB: "valueB"}]})
*/
db.users.find({$or: [{firstName: "Neil"}, {firstName: "Bill"}]});


db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});


/*

	$and operator

	-allows us to find documents amtching multiple criteria in a single field.

	SYNTAX:
		db.collectionName.find({$and: 
		[{fieldA: valueA}, {fieldB: valueB}]
		})
*/

db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

db.users.find({$and: [{age: {$lt: 65}},{age: {$gt: 65}}]});

/*
	FIELD PROJECTION

	-when we retrieve docs, mongoDB usually returns the whole docs
	-there are times when we only oneed specific fields
	-for those cases, we can include or exclude fields from the response


	INCLUSION

	-allows us to include or add specific fields only when retrieving docs
	- we write 1 to incdlude fields

	SYNTAX:
		db.users.find({criteria}, {field:1})

*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
	)


/*
	EXCLUSION

	-allows us to exclude or remove specifi fields when displaying documents
	-the value provided is 0 to denote that the field is being exclueded

	SYNTAX:

		db.users.find({citeria}, {field: 0})


	exclude contact and department for "Jane"

*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		concact: 0,
		department: 0
	}

	)

/*
	SUPRESSING THE ID FIELD

	-ALlows us to exclude the "_id" field when retrieving docs
	-when using field projection, field inclusion and exclusion when not be used at the same time.
	-the "_id is exemppted to this rule"

	SYNTAX
		db.users.find({criteria, {_id:0}})
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}

	)


// EVALUATION OF QUERY OPERATORS

// $regex oerator

/*
	Allows us to find docs that much a specific string pattern using regular expressions

	SYNTAX:
		db.users.find({field: {$regex: 'pattern'}, $options: '$optionsValue'})

*/

// CASE SENSITIVE QUERY
db.users.find({firstName: {$regex: "N"}})

// CASE INSENSITIVE QUERY
db.users.find({firstName: {$regex: "N", $options: "$i"}})